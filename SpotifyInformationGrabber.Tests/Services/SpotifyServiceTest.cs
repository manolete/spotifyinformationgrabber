﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpotifyInformationGrabber.Services;
using System.Linq;

namespace SpotifyInformationGrabber.Tests.Controllers
{
    [TestClass]
    public class SpotifyServiceTest
    {
        string albumJsonString = @"
{
  ""artists"": {
    ""href"": ""https://api.spotify.com/v1/search?query=tania+bowra&offset=0&limit=20&type=artist"",
    ""items"": [ {
      ""external_urls"": {
        ""spotify"": ""https://open.spotify.com/artist/08td7MxkoHQkXnWAYD8d6Q""
      },
      ""genres"": [ ],
      ""href"": ""https://api.spotify.com/v1/artists/08td7MxkoHQkXnWAYD8d6Q"",
      ""id"": ""08td7MxkoHQkXnWAYD8d6Q"",
      ""name"": ""Tania Bowra"",
      ""popularity"": 10,
      ""type"": ""artist"",
      ""uri"": ""spotify:artist:08td7MxkoHQkXnWAYD8d6Q""
    } ],
    ""limit"": 20,
    ""next"": null,
    ""offset"": 0,
    ""previous"": null,
    ""total"": 1
  }
}
";

        [TestMethod]
        public void SpotifySearchResponseIsCorrectlyDecodedIntoObjects()
        {
            var service = new SpotifyService(albumJsonString);
            var artists = service.FindArtistsByName(string.Empty);
            Assert.AreEqual(1, artists.Count(), "Wrong number of artists returned");
            Assert.AreEqual("Tania Bowra", artists.Single().Name, "Incorrect Name in artist");
            Assert.AreEqual("08td7MxkoHQkXnWAYD8d6Q", artists.Single().SpotifyID, "Wrong spotify Id in search result");
        }
    }
}
