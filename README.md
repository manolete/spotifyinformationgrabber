# SpotifyInformationGrabber
A simple .NET MVC using EF to download and display some Spotify Artist and Album Information

A sample .NET MVC app that allows searching for artists, displays them and upon selection shows their albums and songs respectively.
On artist retrieval, the albums are downloaded. When selecting an album, its songs are downloaded also. The downloaded information is stored in the LocalDB.
It tries to use a repository pattern approach to separate DB calls from the controllers. 
In case the webclient connection fails (or something else), a sample error page is shown.

It contains a test project with a sample test to test the deserialization of the returned JSON from spotify.

# Requirements
Requires MS SQL Express with LocalDB 2014 installed. Database is Code-First.
