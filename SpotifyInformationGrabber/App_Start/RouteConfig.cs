﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SpotifyInformationGrabber
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "MyArtists/Details",
                "MyArtists/Details/{spotifyId}",
                new { controller = "MyArtists", action = "Details" },
                new { spotifyId = @"\w+" }
 );

           routes.MapRoute(
                "Albums/Details",
                "Albums/Details/{spotifyId}",
                new { controller = "Albums", action = "Details" },
                new { spotifyId = @"\w+" }
);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
