﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SpotifyInformationGrabber.Startup))]
namespace SpotifyInformationGrabber
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
        }
    }
}
