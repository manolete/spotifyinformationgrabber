﻿using System.Collections.Generic;
using System.Linq;
using SpotifyInformationGrabber.Models;

namespace SpotifyInformationGrabber.DAL.Repositories
{
    public class AlbumRepository : BaseRepository
    {
        public Album FindAlbumBySpotifyId(string spotifyId)
        {
            return context.Albums
                .Include("Songs")
                .Where(a => a.SpotifyID == spotifyId)
                .SingleOrDefault();
        }

        internal void Insert(IEnumerable<Song> songs)
        {
            songs.ToList().ForEach(s => context.Songs.Add(s));
        }
    }
}