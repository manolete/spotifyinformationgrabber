﻿using System;

namespace SpotifyInformationGrabber.DAL.Repositories
{
    public class BaseRepository : IDisposable
    {
        protected SpotifyInformationGrabberContext context;

        public BaseRepository()
        {
            context = new SpotifyInformationGrabberContext();
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

        // Dispose Methods
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}