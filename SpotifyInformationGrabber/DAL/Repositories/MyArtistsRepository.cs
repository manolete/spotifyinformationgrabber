﻿using System.Collections.Generic;
using System.Linq;
using SpotifyInformationGrabber.Models;

namespace SpotifyInformationGrabber.DAL.Repositories
{
    public class MyArtistsRepository : BaseRepository
    {
        public IEnumerable<Artist> GetArtists()
        {
            return context.Artists.ToList();
        }

        public Artist FindArtistBySpotifyId(string spotifyId)
        {
            return context.Artists
                .Include("Albums")
                .Where(a => a.SpotifyID == spotifyId)
                .SingleOrDefault();
        }

        internal void Insert(Artist artist)
        {
            context.Artists.Add(artist);
        }
    }
}