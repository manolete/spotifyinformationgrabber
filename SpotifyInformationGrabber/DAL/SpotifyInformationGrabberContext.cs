﻿using SpotifyInformationGrabber.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace SpotifyInformationGrabber.DAL
{
    public class SpotifyInformationGrabberContext : DbContext
    {

        public SpotifyInformationGrabberContext() : base("SpotifyInformationGrabberContext")
        {
        }

        public DbSet<Artist> Artists { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Song> Songs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}