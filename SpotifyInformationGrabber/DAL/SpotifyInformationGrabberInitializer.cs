﻿using SpotifyInformationGrabber.Models;
using System.Collections.Generic;

namespace SpotifyInformationGrabber.DAL
{
    public class SpotifyInformationGrabberInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<SpotifyInformationGrabberContext>
    {
        protected override void Seed(SpotifyInformationGrabberContext context)
        {
            var artists = new List<Artist>
            {
            };

            artists.ForEach(s => context.Artists.Add(s));
            context.SaveChanges();
            var albums = new List<Album>
            {
            };

            albums.ForEach(s => context.Albums.Add(s));
            context.SaveChanges();

            var songs = new List<Song>
            {
            };
            songs.ForEach(s => context.Songs.Add(s));
            context.SaveChanges();
        }
    }
}