﻿namespace SpotifyInformationGrabber.Models
{
    public class Song
    {
        public int ID { get; set; }
        public string SpotifyID { get; set; }
        public string Name { get; set; }
        public int LengthMs { get; set; }
        public virtual Album Album { get; set; }
    }
}