﻿using System;
using System.Collections.Generic;

namespace SpotifyInformationGrabber.Models
{
    public class Album
    {
        public Album()
        {
            Songs = new List<Song>();
        }

        public int ID { get; set; }
        public string SpotifyID { get; set; }
        public string Name { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int Popularity { get; set; }

        public virtual ICollection<Song> Songs { get; set; }

        public virtual Artist Artist { get; set; }
    }
}