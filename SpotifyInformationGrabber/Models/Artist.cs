﻿using System;
using System.Collections.Generic;

namespace SpotifyInformationGrabber.Models
{
    public class Artist
    {
        public Artist()
        {
            Albums = new List<Album>();
        }

        public int ID { get; set; }
        public string SpotifyID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Album> Albums { get; set; }
    }
}