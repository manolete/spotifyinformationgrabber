﻿using System.Web.Mvc;
using SpotifyInformationGrabber.Models;
using SpotifyInformationGrabber.DAL.Repositories;
using SpotifyInformationGrabber.Services;

namespace SpotifyInformationGrabber.Controllers.MyArtists
{
    public class MyArtistsController : BaseController<MyArtistsRepository>
    {        
        public ActionResult Index()
        {
            var artists = repository.GetArtists();
            return View(artists);
        }

        public ActionResult Details(string spotifyId)
        {
            if (spotifyId == null || spotifyId.Equals(string.Empty)) return Redirect("/");

            Artist artist = repository.FindArtistBySpotifyId(spotifyId);
            if (artist == null)
            {
                var spotifyService = new SpotifyService();
                artist = spotifyService.GetArtist(spotifyId);
                repository.Insert(artist);
                repository.SaveChanges();
            }
            return View(artist);
        }
    }
}
