﻿using System.Web.Mvc;
using SpotifyInformationGrabber.DAL.Repositories;
using SpotifyInformationGrabber.Services;

namespace SpotifyInformationGrabber.Controllers.Search
{
    public class ArtistSearchController : BaseController<MyArtistsRepository>
    {
        readonly SpotifyService spotifyService = new SpotifyService();

        [HttpPost]
        public ActionResult Search()
        {
            var searchString = Request.Form["artistSearch"];
            var artists = spotifyService.FindArtistsByName(searchString);
            return View(artists);
        }
    }
}
