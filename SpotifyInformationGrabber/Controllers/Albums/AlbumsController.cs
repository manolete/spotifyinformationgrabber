﻿using System.Linq;
using System.Web.Mvc;
using SpotifyInformationGrabber.Models;
using SpotifyInformationGrabber.DAL.Repositories;
using SpotifyInformationGrabber.Services;
using MoreLinq;

namespace SpotifyInformationGrabber.Controllers.Albums
{
    public class AlbumsController : BaseController<AlbumRepository>
    {
        public ActionResult Details(string spotifyId)
        {
            if (spotifyId == null || spotifyId.Equals(string.Empty)) return Redirect("/");

            Album album = repository.FindAlbumBySpotifyId(spotifyId);
            if (album == null)
            {
                return HttpNotFound();
            }

            if (!album.Songs.Any())
            {
                var spotifyService = new SpotifyService();
                var songs = spotifyService.GetSongs(album);
                repository.Insert(songs);
                repository.SaveChanges();
            }

            var longestTrack = album.Songs
                .MaxBy(s => s.LengthMs);

            ViewBag.LongestTrack = longestTrack;

            return View(album);
        }
    }
}
