﻿using System;
using System.Web.Mvc;

namespace SpotifyInformationGrabber.Controllers.Home
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}