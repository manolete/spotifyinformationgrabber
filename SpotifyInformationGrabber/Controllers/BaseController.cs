﻿using System;
using System.Web.Mvc;
using SpotifyInformationGrabber.DAL.Repositories;

namespace SpotifyInformationGrabber.Controllers
{
    public class BaseController<TRepository> : Controller where TRepository : BaseRepository
    {
        protected TRepository repository;

        public BaseController()
        {
            repository = (TRepository)Activator.CreateInstance(typeof(TRepository));
        }

        /// <summary>
        /// Constructor for testing only
        /// </summary>
        /// <param name="repository">The mocked repository to use</param>
        public BaseController(TRepository repository)
        {
            this.repository = repository;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
