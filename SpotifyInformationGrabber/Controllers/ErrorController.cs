﻿using System.Web.Mvc;

namespace SpotifyInformationGrabber.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            return View("Error");
        }
    }
}