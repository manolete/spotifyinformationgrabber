﻿using Newtonsoft.Json;
using SpotifyInformationGrabber.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Linq;
using System.Globalization;
using System.Text;

namespace SpotifyInformationGrabber.Services
{
    public class SpotifyService
    {
        private const string spotifySearchUrl = "https://api.spotify.com/v1/search";
        private const string artistUrl = "https://api.spotify.com/v1/artists/";
        private const string albumUrl = "https://api.spotify.com/v1/albums/";
        private readonly NameValueCollection artistTypeValue = new NameValueCollection { { "type", "artist" } };

        public SpotifyService()
        {
        }

        private readonly string mockJsonString = null;
        /// <summary>
        /// Constructor for testing use
        /// </summary>
        /// <param name="mockJsonString">The json string to be parsed</param>
        public SpotifyService(string mockJsonString)
        {
            this.mockJsonString = mockJsonString;
        }

        public Artist GetArtist(string spotifyId)
        {
            using (var client = new WebClient())
            {
                // DownloadString() uses wrong encoding if not specified
                client.Encoding = Encoding.UTF8;
                var url = artistUrl + spotifyId;
                var result = mockJsonString == null ? client.DownloadString(new Uri(url)) : mockJsonString;
                dynamic jsonResult = JsonConvert.DeserializeObject(result);
                Artist artist = GetArtistFromDynamic(jsonResult);
                IEnumerable<Album> albums = GetAlbumsForArtist(artist);

                albums.ToList().ForEach(a => artist.Albums.Add(a));

                return artist;
            }
        }

        public IEnumerable<Album> GetAlbumsForArtist(Artist artist)
        {
            using (var client = new WebClient())
            {
                var albumUrl = string.Format("{0}{1}/albums", artistUrl, artist.SpotifyID);
                string albumsResult = GetResultString(client, albumUrl);
                dynamic jsonAlbumsResult = JsonConvert.DeserializeObject(albumsResult);
                IEnumerable<Album> albums = GetAlbumsFromJsonResult(jsonAlbumsResult, artist);
                return albums;
            }
        }

        private string GetResultString(WebClient client, string albumUrl)
        {
            return mockJsonString == null ? client.DownloadString(new Uri(albumUrl)) : mockJsonString;
        }

        public IEnumerable<Song> GetSongs(Album album)
        {
            using (var client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                var tracksUrl = string.Format("{0}{1}/tracks", albumUrl, album.SpotifyID);
                var tracksResult = GetResultString(client, tracksUrl);
                dynamic jsonTracksResult = JsonConvert.DeserializeObject(tracksResult);
                IEnumerable<Song> songs = GetSongsFromJsonResult(jsonTracksResult, album);
                return songs;
            }
        }

        private static IEnumerable<Song> GetSongsFromJsonResult(dynamic jsonSongResult, Album album)
        {
            foreach (var songResult in jsonSongResult.items)
            {
                yield return new Song()
                {
                    Name = songResult.name,
                    SpotifyID = songResult.id,
                    Album = album,
                    LengthMs = songResult.duration_ms
                };
            }
        }

        private IEnumerable<Album> GetAlbumsFromJsonResult(dynamic jsonAlbumsResult, Artist artist)
        {
            foreach (var albumResult in jsonAlbumsResult.items)
            {
                dynamic fullAlbumJson;
                using (var client = new WebClient())
                {
                    // This is slow. But with the normal link the json does not contain the full album information
                    string fullAlbumLink = albumResult.href;
                    fullAlbumLink = fullAlbumLink
                        .Replace("{", string.Empty)
                        .Replace("}", string.Empty);
                    var albumsResult = GetResultString(client, fullAlbumLink);
                    fullAlbumJson = JsonConvert.DeserializeObject(albumsResult);
                }

                var album = new Album()
                {
                    Name = albumResult.name,
                    SpotifyID = albumResult.id,
                    Artist = artist,
                    Popularity = fullAlbumJson.popularity,
                };

                SetYearAccordingToPrecision(fullAlbumJson, album);

                yield return album;
            }
        }

        private static void SetYearAccordingToPrecision(dynamic fullAlbumJson, Album album)
        {
            string precisionType = fullAlbumJson.release_date_precision;
            string releaseDateString = fullAlbumJson.release_date;
            switch (precisionType)
            {
                case "year":
                    album.ReleaseDate = DateTime.ParseExact(releaseDateString, "yyyy", CultureInfo.InvariantCulture);
                    break;
                case "day":
                    album.ReleaseDate = DateTime.ParseExact(releaseDateString, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    break;
                case "month":
                    album.ReleaseDate = DateTime.ParseExact(releaseDateString, "yyyy-MM", CultureInfo.InvariantCulture);
                    break;
                default:
                    throw new Exception("Unhandled album date precision type");
            }
        }

        private static Artist GetArtistFromDynamic(dynamic artistJson)
        {
            return new Artist()
            {
                SpotifyID = artistJson.id,
                Name = artistJson.name,
            };
        }

        public IEnumerable<Artist> FindArtistsByName(string query)
        {
            using (var client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
                queryString["q"] = System.Web.HttpUtility.HtmlEncode(query);
                queryString.Add(artistTypeValue);

                var url = string.Format("{0}?{1}", spotifySearchUrl, queryString.ToString());
                var result = GetResultString(client, url);
                dynamic jsonResult = JsonConvert.DeserializeObject(result);

                foreach (var artist in jsonResult.artists.items)
                {
                    yield return GetArtistFromDynamic(artist);
                }
            }
        }
    }
}